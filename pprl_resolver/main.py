from functools import lru_cache
from typing import List, Dict

from fastapi import FastAPI, status, HTTPException, Security, Depends
from fastapi.logger import logger
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from pprl import ApiError, BitVector, BitVectorMetadata
from pprl.broker import BrokerClient
from pprl.encoder import EncoderClient
from requests import RequestException

from . import __version__
from .config import Settings
from .datasources import DataSourceBase, MosaicDataSource, MetadataSource
from .model import LocalSessionCacheModel, ExternalServiceHealthModel, HealthResponseModel, \
    ClientCreationRequestModel, new_cache_from_client_creation_request, PseudonymMatchResponseModel, \
    BitVectorMatchModel, BitVectorModel, BitVectorMetadataModel, VersionResponseModel
from .util import get_external_service_health_status


@lru_cache
def get_settings():
    return Settings()


@lru_cache
def get_data_source(settings: Settings = Depends(get_settings)) -> DataSourceBase:
    match settings.data_source.lower():
        case "mosaic":
            return MosaicDataSource(settings)
        case _:
            raise ValueError(f"Unknown data source '{settings.data_source}'")


@lru_cache
def get_broker_client(settings: Settings = Depends(get_settings)) -> BrokerClient:
    return BrokerClient(settings.broker_base_url)


@lru_cache
def get_encoder_client(settings: Settings = Depends(get_settings)) -> EncoderClient:
    return EncoderClient(settings.encoder_base_url)


app = FastAPI(
    title="Pseudonym resolver API",
    version=__version__,
    description="The pseudonym resolver is a service for collecting pseudonymized data from various sources, "
                "encoding it using state-of-the-art PPRL techniques and sending the resulting bit vectors to a "
                "central linkage unit. The whole client side is managed by the resolver and results are returned in "
                "a pseudonymized form.",
    openapi_tags=[
        {
            "name": "info",
            "description": "Generic information about the state of the pseudonym resolver service"
        },
        {
            "name": "session",
            "description": "Session management operations handled by the pseudonym resolver service"
        }
    ]
)

bearer_security = HTTPBearer(description="Session secret obtained from broker")
global_session_dict: Dict[str, LocalSessionCacheModel] = {}


@app.get(
    "/version",
    response_model=VersionResponseModel,
    summary="Returns the version of the pseudonym resolver service",
    tags=["info"]
)
async def get_version():
    logger.debug("GET /version")
    return VersionResponseModel(version=__version__)


@app.get(
    "/attributes",
    response_model=List[str],
    summary="Returns the list of attributes supported by the configured data source",
    tags=["info"]
)
async def get_available_attributes(data_source: DataSourceBase = Depends(get_data_source)) -> List[str]:
    logger.debug("GET /attributes")
    return data_source.get_available_attributes()


@app.get(
    "/sources",
    response_model=List[str],
    summary="Returns the list of metadata sourced supported by the configured data source",
    tags=["info"]
)
async def get_available_metadata_sources(data_source: DataSourceBase = Depends(get_data_source)) -> List[str]:
    logger.debug("GET /sources")
    return data_source.get_available_metadata_sources()


@app.get(
    "/health",
    response_model=HealthResponseModel,
    response_model_exclude_unset=True,
    summary="Returns the health status of the pseudonym resolver service",
    description="This endpoint queries the health status of the pseudonym resolver service (which should always be "
                "\"HEALTHY\"). In addition, if external services should be queried too, they will be contacted as "
                "well to test them on their availability and responsiveness. There are three possible health statuses "
                "that a service can take on. If a service is healthy, it means that a connection has been established "
                "and the service is operating as expected. If a service is unhealthy, it means that a connection has "
                "been established, but the service returned an unexpected response. If a service is unavailable, it "
                "means that a connection couldn't be established. Service health status is always queried live.",
    tags=["info"]
)
async def check_service_health(
        external: bool = False,
        settings: Settings = Depends(get_settings),
        data_source: DataSourceBase = Depends(get_data_source)
):
    logger.debug("GET /health")
    r = HealthResponseModel(status="HEALTHY")

    if external:
        logger.debug("External service health info requested")
        r.external = []

        for uri in (settings.encoder_base_url, settings.broker_base_url):
            r.external.append(ExternalServiceHealthModel(
                uri=uri,
                status=get_external_service_health_status(uri)
            ))

        for uri, srv_status in data_source.test_external_service_health().items():
            r.external.append(ExternalServiceHealthModel(
                uri=uri,
                status=srv_status
            ))

    return r


@app.get(
    "/",
    response_model=PseudonymMatchResponseModel,
    summary="Returns a list of pseudonymized matches for vectors submitted by this service",
    description="This endpoint contacts the broker and retrieves results for bit vectors that this service has "
                "previously submitted. The matched pseudonyms can be gathered by inspecting the IDs of the bit vectors "
                "returned by this endpoint.",
    tags=["session"]
)
async def fetch_results(
        credentials: HTTPAuthorizationCredentials = Security(bearer_security),
        broker: BrokerClient = Depends(get_broker_client)
):
    logger.debug("GET /")

    session_secret = credentials.credentials

    if session_secret not in global_session_dict:
        logger.warning("Authorization header provided, but session is not present in cache")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Session is not registered at the resolver, use POST first")

    session_cache = global_session_dict[session_secret]
    client_secret = session_cache.client_secret

    try:
        match_results = broker.get_results(client_secret)
    except ApiError:
        logger.error("Failed to get results from broker", exc_info=True)
        raise HTTPException(status.HTTP_502_BAD_GATEWAY, "Couldn't fetch results from broker")

    return PseudonymMatchResponseModel(match_list=[
        BitVectorMatchModel(
            vector=BitVectorModel(
                id=session_cache.entity_id_to_psn_map[m.vector.id],
                value=m.vector.value,
                metadata=[
                    BitVectorMetadataModel.from_orm(d) for d in m.vector.metadata
                ]
            ),
            similarity=m.similarity,
            reference_metadata=[
                BitVectorMetadataModel.from_orm(d) for d in m.reference_metadata
            ]
        ) for m in match_results
    ])


@app.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    summary="Registers this pseudonym resolver as a client for a match session",
    description="This endpoint registers itself as a participating client in a match session. The session secret is "
                "sourced from the authorization header contained in the request. A number of checks will be performed "
                "to check the integrity of the incoming request. Particularly, the session secret may not have been "
                "reused earlier, and there must be at least one attribute supported by the configured data source "
                "within the request.",
    tags=["session"]
)
async def create_client(
        request: ClientCreationRequestModel,
        credentials: HTTPAuthorizationCredentials = Security(bearer_security),
        broker: BrokerClient = Depends(get_broker_client),
        data_source: DataSourceBase = Depends(get_data_source)
):
    session_secret = credentials.credentials

    if session_secret in global_session_dict:
        logger.warning("Authorization header provided, but session is already present in cache")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Session is already registered")

    # check that any attributes have been submitted
    if len(request.client_config.attributes) == 0:
        logger.warning("Request is well-formed, but doesn't contain any attributes")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "No attributes specified")

    # check that all submitted attributes are in fact valid
    for a in request.client_config.attributes:
        if a not in data_source.get_available_attributes():
            logger.warning(f"Request defines an attribute '{a}' that is not recognized by the data source")
            raise HTTPException(status.HTTP_400_BAD_REQUEST, f"Unrecognized attribute: '{a}'")

    # check that all metadata specifications are valid
    for s in request.metadata_spec_list:
        if s.source not in data_source.get_available_metadata_sources():
            logger.warning(f"Request defines a metadata source '{s.source}' that is not recognized by the data source")
            raise HTTPException(status.HTTP_400_BAD_REQUEST, f"Unrecognized metadata source: '{s.source}'")

    try:
        client_secret = broker.create_client(session_secret)
    except ApiError as e:
        logger.error("Failed to create client at broker", exc_info=True)
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, f"Failed to register session: {str(e)}")
    except RequestException as e:
        logger.error("Failed to connect to broker", exc_info=True)
        raise HTTPException(status.HTTP_502_BAD_GATEWAY, f"Failed to connect to broker: {str(e)}")

    global_session_dict[session_secret] = new_cache_from_client_creation_request(request, client_secret)


@app.put(
    "/",
    status_code=status.HTTP_202_ACCEPTED,
    summary="Submits pseudonyms to a match session",
    description="This endpoint submits pseudonyms to a match session. The resolver uses the session secret contained "
                "in the authorization header to look up the client secret that was created by a previous POST request "
                "targeted at this service. In essence, the resolver runs through a series of steps. First, the "
                "pseudonyms are handed off to the configured data source. It resolves the pseudonyms into the original "
                "identifiers and creates a new record given the associated data referenced by the attributes in the "
                "initial POST request. These records are then assigned a new, random identifier, encoded and sent off "
                "to the broker. The mapping of random identifier to pseudonym is stored locally and reused when "
                "collecting results later. This endpoint can be called multiple times to submit batches of pseudonyms "
                "to a match session.",
    tags=["session"]
)
async def submit_pseudonyms(
        pseudonym_list: List[str],
        credentials: HTTPAuthorizationCredentials = Security(bearer_security),
        broker: BrokerClient = Depends(get_broker_client),
        encoder: EncoderClient = Depends(get_encoder_client),
        data_source: DataSourceBase = Depends(get_data_source)
):
    session_secret = credentials.credentials

    if session_secret not in global_session_dict:
        logger.warning("Authorization header provided, but session is not present in cache")
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Session is not registered at the resolver, use POST first")

    session_cache = global_session_dict[session_secret]

    # prepare args for datasource
    domain_name = session_cache.client_config.domain
    attributes = session_cache.client_config.attributes
    metadata_source_list = [
        MetadataSource(
            name=s.name,
            source=s.source,
            data_type=s.data_type
        ) for s in session_cache.metadata_spec_list
    ]

    try:
        entity_list = data_source.resolve(pseudonym_list, attributes, metadata_source_list, domain_name)
        _entity_id_to_metadata_dict: Dict[str, List[BitVectorMetadata]] = {
            e.entity.id: e.metadata for e in entity_list
        }
    except (ConnectionError, ValueError) as e:
        logger.error("Failed to resolve pseudonyms", exc_info=True)
        raise HTTPException(status.HTTP_502_BAD_GATEWAY, f"Data source failed to resolve pseudonyms: {str(e)}")

    try:
        encoded_entity_list = encoder.encode(
            config=session_cache.bloom_filter.to_bloom_filter(),
            schema_list=[s.to_attribute_schema() for s in session_cache.schema_list],
            entity_list=[e.entity for e in entity_list])
    except ApiError:
        logger.error("Failed to encode entities", exc_info=True)
        raise HTTPException(status.HTTP_502_BAD_GATEWAY, "Failed to encode entities")

    try:
        broker.submit_bit_vectors(session_cache.client_secret, [
            BitVector(
                id=e.id,
                value=e.value,
                metadata=[m for m in _entity_id_to_metadata_dict[e.id]]
            ) for e in encoded_entity_list
        ])
    except ApiError:
        logger.error("Failed to send entities to broker", exc_info=True)
        raise HTTPException(status.HTTP_502_BAD_GATEWAY, "Failed to submit bit vectors")

    # now that everything is submitted, we can patch the cache and be done with everything
    global_session_dict[session_secret].entity_id_to_psn_map.update({
        e.entity.id: e.pseudonym for e in entity_list
    })
