from typing import List, Union

import requests
from requests import RequestException


def _snake_to_camel(s: str) -> str:
    s = s.strip("_")  # remove leading and trailing underscores
    s_parts = s.split("_")

    for i in range(1, len(s_parts)):
        s_parts[i] = s_parts[i].capitalize()

    return "".join(s_parts)


def snake_to_camel(str_in: Union[str, List[str]]) -> Union[str, List[str]]:
    """
    Converts a string from snake_case to camelCase.

    :param str_in: snake-cased string or list of strings
    :return: camel-cased string or list of strings
    """
    if type(str_in) == list:
        # noinspection PyTypeChecker
        return [_snake_to_camel(s) for s in str_in]
    else:
        return _snake_to_camel(str_in)


def _camel_to_snake(s: str) -> str:
    s_out = s[:1].lower()

    for c in s[1:]:
        if c.isupper():
            c = c.lower()
            s_out += "_"

        s_out += c

    return s_out


def camel_to_snake(str_in: Union[str, List[str]]) -> Union[str, List[str]]:
    """
    Converts a string from camelCase to snake_case.

    :param str_in: camel-cased string or list of strings
    :return: snake-cased string or list of strings
    """
    if type(str_in) == list:
        # noinspection PyTypeChecker
        return [_camel_to_snake(s) for s in str_in]
    else:
        return _camel_to_snake(str_in)


def get_external_service_health_status(url: str, expected_status: int = 200, timeout_secs: int = 5) -> str:
    try:
        r = requests.get(url, timeout=timeout_secs)

        if r.status_code == expected_status:
            result_status = "HEALTHY"
        else:
            result_status = "UNHEALTHY"
    except RequestException:
        result_status = "UNAVAILABLE"

    return result_status
