from pydantic import BaseSettings


class Settings(BaseSettings):
    broker_base_url: str = "http://localhost:8080/broker"
    encoder_base_url: str = "http://localhost:8080/encoder"
    epix_wsdl_url: str = "http://localhost:8080/epix/epixService?wsdl"
    gpas_wsdl_url: str = "http://localhost:8080/gpas/gpasService?wsdl"
    data_source: str = "mosaic"

    def __hash__(self):
        return hash((type(self),) + tuple(self.__dict__.values()))
