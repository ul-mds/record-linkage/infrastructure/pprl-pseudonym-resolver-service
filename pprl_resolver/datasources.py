import datetime
import secrets
from typing import List, NamedTuple, Dict, Any

import requests.exceptions
from mosaic_client.epix import EPIXClient
from mosaic_client.gpas import GPASClient
from pprl import Entity, BitVectorMetadata
from zeep.exceptions import Fault, Error

from .config import Settings
from .util import snake_to_camel, get_external_service_health_status, camel_to_snake


def _prepare_value_for_encoder(value: Any) -> str:
    if value is None:
        return ""

    t = type(value)

    if t == datetime.datetime:
        return value.strftime("%Y-%m-%d %H:%M:%S")
    elif t == str:
        return value
    else:
        raise ValueError(f"Unsupported datatype for E-PIX identity/contact: {t}")


class AnnotatedEntity(NamedTuple):
    pseudonym: str
    entity: Entity
    metadata: List[BitVectorMetadata]


class MetadataSource(NamedTuple):
    name: str
    source: str
    data_type: str

    def to_metadata(self, value: str) -> BitVectorMetadata:
        return BitVectorMetadata(name=self.name, value=value)


class DataSourceBase:

    def __init__(self, settings: Settings):
        self.settings = settings

    def test_external_service_health(self) -> Dict[str, str]:
        raise NotImplementedError()

    def get_available_attributes(self) -> List[str]:
        raise NotImplementedError()

    def get_available_metadata_sources(self) -> List[str]:
        raise NotImplementedError()

    def resolve(
            self,
            psn_list: List[str],
            attributes: List[str],
            metadata_source_list: List[MetadataSource],
            domain: str
    ) -> List[AnnotatedEntity]:
        raise NotImplementedError()


class MosaicDataSource(DataSourceBase):

    def __init__(self, settings: Settings):
        super().__init__(settings)

        self.__epix = None
        self.__gpas = None

    def get_epix_client(self) -> EPIXClient:
        if self.__epix is not None:
            return self.__epix

        try:
            self.__epix = EPIXClient(self.settings.epix_wsdl_url)
            return self.__epix
        except (Error, requests.exceptions.RequestException):
            raise ConnectionError(f"Failed to connect to E-PIX service at {self.settings.epix_wsdl_url}")

    def get_gpas_client(self) -> GPASClient:
        if self.__gpas is not None:
            return self.__gpas

        try:
            self.__gpas = GPASClient(self.settings.gpas_wsdl_url)
            return self.__gpas
        except (Error, requests.exceptions.RequestException):
            raise ConnectionError(f"Failed to connect to gPAS service at {self.settings.gpas_wsdl_url}")

    def test_external_service_health(self) -> Dict[str, str]:
        health_dict: Dict[str, str] = {}

        for u in (self.settings.epix_wsdl_url, self.settings.gpas_wsdl_url):
            health_dict[u] = get_external_service_health_status(u)

        return health_dict

    def get_available_metadata_sources(self) -> List[str]:
        from mosaic_client import Person
        from dataclasses import fields
        from datetime import datetime

        sources = [f.name for f in fields(Person) if f.type == datetime]

        return [snake_to_camel(s) for s in sources]

    def get_available_attributes(self) -> List[str]:
        from mosaic_client import Identity, Contact
        from dataclasses import fields

        attrs = [f.name for f in fields(Identity) if f.name not in ("identifiers", "contacts")]
        attrs += [f"contact.{f.name}" for f in fields(Contact)]

        attrs.sort()

        return [snake_to_camel(a) for a in attrs]

    def resolve(
            self,
            psn_list: List[str],
            attributes: List[str],
            metadata_source_list: List[MetadataSource],
            domain: str
    ) -> List[AnnotatedEntity]:
        metadata_source_list = [
            MetadataSource(
                name=m.name,
                source=camel_to_snake(m.source),
                data_type=m.data_type
            ) for m in metadata_source_list
        ]

        epix = self.get_epix_client()
        gpas = self.get_gpas_client()

        try:
            psn_mpi_pairs = gpas.get_value_for_list(domain, psn_list)
            mpi_id_list = [p[1] for p in psn_mpi_pairs]

            # temporary mapping of mpi to psn, so that we can do entity id -> mpi -> psn later
            _mpi_to_psn_dict: Dict[str, str] = {p[1]: p[0] for p in psn_mpi_pairs}
        except Fault as e:
            raise ConnectionError("Failed to resolve pseudonyms - are they correct and is the domain name valid? "
                                  f"({str(e)})")

        try:
            person_list = [epix.get_person_by_mpi(domain, mpi) for mpi in mpi_id_list]
        except Fault as e:
            raise ConnectionError(f"Failed to resolve MPIs - have they been correctly pseudonymized? ({str(e)})")

        contact_prefix = "contact."
        result_list: List[AnnotatedEntity] = []

        for person in person_list:
            identity = person.reference_identity
            psn = _mpi_to_psn_dict[person.mpi()]

            # create a new entity with unique ID
            entity_id = secrets.token_urlsafe(32)
            entity = Entity(id=entity_id)
            # create empty list of metadata
            metadata_list: List[BitVectorMetadata] = []

            # collect metadata for person
            for metadata_src in metadata_source_list:
                spec_value = getattr(person, camel_to_snake(metadata_src.source), None)

                # check if the value has been set
                if spec_value is None:
                    raise ValueError(f"Mandatory metadata field '{metadata_src.name}' is not set")

                # if datetime, convert to ISO string
                if type(spec_value) == datetime.datetime:
                    spec_value = spec_value.isoformat(timespec="seconds")
                else:
                    spec_value = str(spec_value)

                metadata_list.append(metadata_src.to_metadata(spec_value))

            # collect attributes from identity
            for attribute_name in attributes:
                # check if it's a contact attribute
                if attribute_name.startswith(contact_prefix):
                    # trim off contact prefix
                    attribute_name = attribute_name[len(contact_prefix):]

                    # iterate over all contacts assigned to this identity
                    for i in range(len(identity.contacts)):
                        contact = identity.contacts[i]
                        # the key becomes contact[<index>].<attribute>
                        contact_attribute_name = f"contact[{i}].{attribute_name}"

                        attribute_value = getattr(contact, camel_to_snake(attribute_name), None)
                        entity.attributes[contact_attribute_name] = _prepare_value_for_encoder(attribute_value)
                else:  # this is for when it's a "normal" top-level attribute
                    attribute_value = getattr(identity, camel_to_snake(attribute_name), None)
                    entity.attributes[attribute_name] = _prepare_value_for_encoder(attribute_value)

            result_list.append(AnnotatedEntity(
                pseudonym=psn,
                entity=entity,
                metadata=metadata_list
            ))

        return result_list
