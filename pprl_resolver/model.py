from typing import List, Union, Dict

from . import __version__
from pprl import BloomFilterConfiguration, AttributeSchema, BitVectorMetadata
from pydantic import BaseModel, Field


class BitVectorMetadataModel(BaseModel):
    name: str
    value: str

    class Config:
        orm_mode = True

    def to_metadata(self) -> BitVectorMetadata:
        return BitVectorMetadata(name=self.name, value=self.value)


class BitVectorModel(BaseModel):
    id: str
    value: str
    metadata: List[BitVectorMetadataModel]

    class Config:
        orm_mode = True


class BitVectorMetadataSpecificationModel(BaseModel):
    name: str
    data_type: str = Field(alias="dataType")
    decision_rule: str = Field(alias="decisionRule")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True

    def as_metadata(self, value: str) -> BitVectorMetadataModel:
        return BitVectorMetadataModel(
            name=self.name,
            value=value
        )


class ResolverBitVectorMetadataSpecificationModel(BitVectorMetadataSpecificationModel):
    source: str


class BitVectorMatchModel(BaseModel):
    vector: BitVectorModel
    similarity: float
    reference_metadata: List[BitVectorMetadataModel] = Field(alias="referenceMetadata")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class BloomFilterConfigurationModel(BaseModel):
    charset: Union[str, None] = None
    filter_type: Union[str, None] = Field(default=None, alias="filterType")
    hash_strategy: Union[str, None] = Field(default=None, alias="hashStrategy")
    hash_values: Union[int, None] = Field(default=None, alias="hashValues")
    token_size: Union[int, None] = Field(default=None, alias="tokenSize")
    seed: Union[int, None] = None
    key: Union[str, None] = None
    salt: Union[str, None] = None

    class Config:
        orm_mode = True
        allow_population_by_field_name = True

    def to_bloom_filter(self) -> BloomFilterConfiguration:
        return BloomFilterConfiguration(
            charset=self.charset,
            filter_type=self.filter_type,
            hash_strategy=self.hash_strategy,
            hash_values=self.hash_values,
            token_size=self.token_size,
            seed=self.seed,
            key=self.key,
            salt=self.salt
        )


class AttributeSchemaModel(BaseModel):
    attribute_name: str = Field(alias="attributeName")
    data_type: str = Field(alias="dataType")
    average_token_count: float = Field(alias="averageTokenCount")
    weight: float

    class Config:
        orm_mode = True
        allow_population_by_field_name = True

    def to_attribute_schema(self) -> AttributeSchema:
        return AttributeSchema(
            attribute_name=self.attribute_name,
            data_type=self.data_type,
            average_token_count=self.average_token_count,
            weight=self.weight
        )


class ClientConfigurationModel(BaseModel):
    domain: str
    attributes: List[str]


class ClientCreationRequestModel(BaseModel):
    bloom_filter: BloomFilterConfigurationModel = Field(alias="bloomFilter")
    client_config: ClientConfigurationModel = Field(alias="clientConfig")
    schema_list: List[AttributeSchemaModel] = Field(alias="schemas")
    metadata_spec_list: List[ResolverBitVectorMetadataSpecificationModel] = Field(alias="metadataSpecification")

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "bloomFilter": {
                    "charset": "UTF-8",
                    "filterType": "RBF",
                    "hashStrategy": "RANDOM_SHA256",
                    "hashValues": 5,
                    "tokenSize": 2,
                    "seed": 1337,
                    "key": "s3cr3t",
                    "salt": "s4lty"
                },
                "clientConfig": {
                    "domain": "default",
                    "attributes": ["firstName", "lastName", "birthDate", "gender"]
                },
                "schemas": [
                    {
                        "attributeName": "firstName",
                        "dataType": "string",
                        "averageTokenCount": 10,
                        "weight": 3
                    },
                    {
                        "attributeName": "lastName",
                        "dataType": "string",
                        "averageTokenCount": 8,
                        "weight": 3
                    },
                    {
                        "attributeName": "birthDate",
                        "dataType": "datetime",
                        "averageTokenCount": 10,
                        "weight": 2
                    },
                    {
                        "attributeName": "gender",
                        "dataType": "char",
                        "averageTokenCount": 2,
                        "weight": 2
                    }
                ],
                "metadataSpecification": [
                    {
                        "name": "createdAt",
                        "dataType": "datetime",
                        "decisionRule": "keepLatest",
                        "source": "personCreated"
                    }
                ]
            }
        }

    def to_local_session_cache(self, client_secret: str):
        return LocalSessionCacheModel(
            bloom_filter=self.bloom_filter,
            client_config=self.client_config,
            schema_list=self.schema_list,
            metadata_spec_list=self.metadata_spec_list,
            client_secret=client_secret
        )


class LocalSessionCacheModel(ClientCreationRequestModel):
    client_secret: str = Field(alias="clientSecret")
    entity_id_to_psn_map: Dict[str, str] = Field(alias="entityIDToPSNMap", default_factory=dict)


def new_cache_from_client_creation_request(req: ClientCreationRequestModel,
                                           client_secret: str) -> LocalSessionCacheModel:
    return LocalSessionCacheModel(
        bloom_filter=req.bloom_filter,
        client_config=req.client_config,
        schema_list=req.schema_list,
        metadata_spec_list=req.metadata_spec_list,
        client_secret=client_secret
    )


class PseudonymSubmissionRequestModel(BaseModel):
    pseudonym_list: List[str] = Field(alias="pseudonyms")

    class Config:
        allow_population_by_field_name = True


class PseudonymMatchResponseModel(BaseModel):
    match_list: List[BitVectorMatchModel] = Field(alias="matches")

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "matches": [
                    {
                        "vector": {
                            "id": "b8fd96b24f349d83ef0a0acc6badd445",
                            "value": "KOXorb6Z5Nn7mMJs",
                            "metadata": [
                                {
                                    "name": "createdAt",
                                    "value": "2022-06-28T09:10:05+02:00"
                                }
                            ]
                        },
                        "similarity": 0.98,
                        "referenceMetadata": [
                            {
                                "name": "createdAt",
                                "value": "2022-06-28T10:02:32+02:00"
                            }
                        ]
                    }
                ]
            }
        }


class ExternalServiceHealthModel(BaseModel):
    uri: str
    status: str


class HealthResponseModel(BaseModel):
    status: str
    external: Union[List[ExternalServiceHealthModel], None] = None

    class Config:
        schema_extra = {
            "example": {
                "status": "HEALTHY",
                "external": [
                    {
                        "uri": "http://other.service/here",
                        "status": "HEALTHY"
                    }
                ]
            }
        }


class VersionResponseModel(BaseModel):
    version: str

    class Config:
        schema_extra = {
            "example": {
                "version": __version__
            }
        }
