# PPRL Resolver Service

The PPRL pseudonym resolver service handles the client-side stuff in a PPRL workflow.
It connects to different data sources, converts pseudonyms into data records, encodes them and sends them off to a central linkage unit.
Match results are returned in a pseudonymized form.

## Running with Uvicorn

The pseudonym resolver can be run with any ASGI web server.
This example will show the startup using [Uvicorn](https://www.uvicorn.org/).
It is highly recommended that you use Python 3.10 on the machine that you want to host the resolver on.
First, clone this repository.
Then, install Uvicorn on your machine by running `python -m pip install uvicorn`.
Make sure to set necessary [environment variables](#setting-environment-variables).
Navigate to the root directory of this repository and execute the following line.

```
uvicorn pprl_resolver.main:app --host 0.0.0.0 --port 8080
```

This will make the web server listen on port 8080 of all interfaces and forward the requests to the pseudonym resolver.

## Running with Docker

The pseudonym resolver can also be run with a preconfigured Docker image.
All you need to do is to download it and create a container with it.

```
docker pull mds4ul/pprl-resolver:0.6.0
```

By default, the container exposes the service on port 8080.
Don't forget to bind it to your host machine, to use a different port or to put it behind a proxy, if necessary.
Make sure to set [environment variables](#setting-environment-variables) accordingly.

```
docker run -p 8080:8080 \
    -e BROKER_BASE_URL="http://localhost:8080/broker" \
    -e ENCODER_BASE_URL="http://localhost:8080/encoder" \
    -e EPIX_WSDL_URL="http://localhost:8080/epix/epixService?wsdl" \
    -e GPAS_WSDL_URL="http://localhost:8080/gpas/gpasService?wsdl" \
    -e DATA_SOURCE="mosaic" \
    --name pprl-resolver \
    -d \
    mds4ul/pprl-resolver:0.6.0
```

## Exploring the API

The service is built using [FastAPI](https://fastapi.tiangolo.com/).
This means that it comes equipped with a self-documenting and interactive documentation.
Simply navigate to the `/docs` path on the URL where you hosted the pseudonym resolver.

## Setting environment variables

**Name** | **Description** | **Default**
:--|:--|:--
`BROKER_BASE_URL` | URL at which a PPRL broker service is hosted | http://localhost:8080/broker
`ENCODER_BASE_URL` | URL at which a PPRL encoder service is hosted | http://localhost:8080/encoder
`EPIX_WSDL_URL` | WSDL endpoint of a hosted E-PIX service<sup>(1)</sup>| http://localhost:8080/epix/epixService?wsdl
`GPAS_WSDL_URL` | WSDL endpoint of a hosted gPAS service<sup>(1)</sup> | http://localhost:8080/gpas/gpasService?wsdl
`DATA_SOURCE` | Data source implementation to use<sup>(2)</sup> | mosaic

<sup>(1)</sup> This is only necessary if `DATA_SOURCE` is set to mosaic.  
<sup>(2)</sup> This is the list of all acceptable values: mosaic.

## Running tests

Run the linter in the root directory using `poetry run flake8`.

Navigate to the [tests](./tests) directory on the command line and execute `docker compose up -d`.
This will start a number of services that are required to run the integration tests.
Once they're up and running (might take a couple minutes), run the following command in the root directory of this repository.

```
PYTEST_EPIX_WSDL_URL="http://localhost:8080/epix/epixService?wsdl" \
    PYTEST_GPAS_WSDL_URL="http://localhost:8080/gpas/gpasService?wsdl" \
    PYTEST_BROKER_BASE_URL="http://localhost:8080/broker" \
    PYTEST_ENCODER_BASE_URL="http://localhost:8080/encoder" \
    poetry run pytest
```

## License

MIT.