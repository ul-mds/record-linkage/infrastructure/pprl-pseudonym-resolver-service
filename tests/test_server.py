import datetime
import secrets
import time
from typing import Dict, List

import pytest
from faker import Faker
from fastapi import status
from fastapi.testclient import TestClient
from mosaic_client import Identity, Contact
from mosaic_client.epix import EPIXClient
from mosaic_client.gpas import GPASClient
from pprl import MatchConfiguration, BitVector, BitVectorMetadata, BitVectorMetadataSpecification
from pprl.broker import BrokerClient

from pprl_resolver import __version__
from pprl_resolver.main import app, get_settings
from pprl_resolver.model import ClientCreationRequestModel, BloomFilterConfigurationModel, ClientConfigurationModel, \
    AttributeSchemaModel, ResolverBitVectorMetadataSpecificationModel, HealthResponseModel, \
    PseudonymMatchResponseModel, VersionResponseModel
from .test_config import get_domain_name, get_epix_wsdl_url, get_gpas_wsdl_url, get_broker_base_url, \
    get_epix_source_name, get_settings_override, get_service_timeout_secs

pytestmark = pytest.mark.integration
client = TestClient(app)


def auth(bearer: str) -> Dict[str, str]:
    return {"Authorization": f"Bearer {bearer}"}


_client_creation_request = ClientCreationRequestModel(
    bloom_filter=BloomFilterConfigurationModel(
        charset="UTF-8",
        filter_type="RBF",
        hash_strategy="RANDOM_SHA256",
        hash_values=5,
        token_size=2,
        seed=1337,
        key="s3cr3t",
        salt="foobar"
    ),
    client_config=ClientConfigurationModel(
        domain=get_domain_name(),
        attributes=["firstName", "lastName", "gender", "birthDate"]
    ),
    schema_list=[
        AttributeSchemaModel(
            attribute_name="firstName",
            data_type="string",
            average_token_count=10,
            weight=1
        ),
        AttributeSchemaModel(
            attribute_name="lastName",
            data_type="string",
            average_token_count=10,
            weight=1
        ),
        AttributeSchemaModel(
            attribute_name="gender",
            data_type="string",
            average_token_count=10,
            weight=1
        ),
        AttributeSchemaModel(
            attribute_name="birthDate",
            data_type="datetime",
            average_token_count=10,
            weight=1
        ),
    ],
    metadata_spec_list=[
        ResolverBitVectorMetadataSpecificationModel(
            name="createdAt",
            data_type="datetime",
            decision_rule="keepLatest",
            source="personCreated"
        )
    ]
)

app.dependency_overrides[get_settings] = get_settings_override


@pytest.fixture(scope="module")
def epix_client() -> EPIXClient:
    return EPIXClient(get_epix_wsdl_url())


@pytest.fixture(scope="module")
def gpas_client() -> GPASClient:
    return GPASClient(get_gpas_wsdl_url())


@pytest.fixture(scope="module")
def fake() -> Faker:
    fake = Faker()
    Faker.seed(0)

    return fake


@pytest.fixture
def session_secret():
    broker = BrokerClient(get_broker_base_url())
    session_secret, _ = broker.create_session(
        config=MatchConfiguration(
            match_function="JACCARD",
            threshold=0
        ),
        session_cancellation="SIMPLE",
        metadata_specifications=[
            BitVectorMetadataSpecification(
                name=s.name,
                data_type=s.data_type,
                decision_rule=s.decision_rule
            ) for s in _client_creation_request.metadata_spec_list
        ]
    )

    yield session_secret
    broker.cancel_session(session_secret)


@pytest.fixture
def pseudonyms(epix_client: EPIXClient, gpas_client: GPASClient, fake: Faker) -> List[str]:
    identity_list: List[Identity] = []

    for _ in range(3):
        identity_list.append(Identity(
            birth_date=fake.date_time(),
            birth_place=fake.city(),
            external_date=fake.date_time(),
            first_name=fake.first_name(),
            gender=fake.random_element(['M', 'F']),
            last_name=fake.last_name(),
            mother_tongue=fake.language_name(),
            mothers_maiden_name=fake.last_name(),
            contacts=[
                Contact(
                    city=fake.city(),
                    country=fake.country(),
                    country_code=fake.country_code(),
                    email=fake.email(),
                    external_date=fake.date_time(),
                    phone=fake.phone_number(),
                    street=fake.address(),
                    zip_code=fake.postcode()
                )
            ]
        ))

    mpi_responses = epix_client.request_mpi_batch(get_domain_name(), get_epix_source_name(), identity_list)
    mpi_ids = [m.person.mpi() for m in mpi_responses]

    mpi_psn_list = gpas_client.get_or_create_pseudonym_for_list(get_domain_name(), mpi_ids)
    psn_list = [p[1] for p in mpi_psn_list]

    yield psn_list

    gpas_client.delete_entries(get_domain_name(), mpi_ids)

    for mpi_id in mpi_ids:
        epix_client.deactivate_person(get_domain_name(), mpi_id)
        epix_client.delete_person(get_domain_name(), mpi_id)


def test_health():
    r = client.get("/health")
    assert r.status_code == status.HTTP_200_OK

    e = HealthResponseModel(**r.json())
    assert e.status == "HEALTHY"


def test_attributes():
    r = client.get("/attributes")
    assert r.status_code == status.HTTP_200_OK

    j = r.json()
    assert len(j) != 0


def test_external_service_health():
    r = client.get("/health", params={"external": True})
    assert r.status_code == status.HTTP_200_OK

    e = HealthResponseModel(**r.json())

    assert e.status == "HEALTHY"
    assert len(e.external) != 0

    for external_health in e.external:
        assert external_health.status == "HEALTHY"


def test_version():
    r = client.get("/version")
    assert r.status_code == status.HTTP_200_OK

    e = VersionResponseModel(**r.json())
    assert e.version == __version__


def test_post_forbidden_no_bearer():
    r = client.post("/")
    assert r.status_code == status.HTTP_403_FORBIDDEN


def test_get_forbidden_no_bearer():
    r = client.get("/")
    assert r.status_code == status.HTTP_403_FORBIDDEN


def test_put_forbidden_no_bearer():
    r = client.put("/")
    assert r.status_code == status.HTTP_403_FORBIDDEN


def test_put_validation_error_no_body():
    r = client.put("/", headers=auth("foobar"))
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_put_bad_request_not_registered():
    r = client.put("/", json=[], headers=auth("foobar"))
    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_post_validation_error_no_body():
    r = client.post("/", headers=auth("foobar"))
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_post_validation_error_empty_obj():
    r = client.post("/", json={}, headers=auth("foobar"))
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_post_bad_request_no_attributes():
    c = _client_creation_request.copy(deep=True)
    c.client_config.attributes = []

    r = client.post("/", json=c.dict(), headers=auth("foobar"))
    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_post_bad_request_invalid_attribute():
    c = _client_creation_request.copy(deep=True)
    c.client_config.attributes += ["foobar"]

    r = client.post("/", json=c.dict(by_alias=True), headers=auth("foobar"))
    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_post_bad_request_invalid_metadata_source():
    c = _client_creation_request.copy(deep=True)
    c.metadata_spec_list[0].source = "foobar"

    r = client.post("/", json=c.dict(by_alias=True), headers=auth("foobar"))
    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_post(session_secret: str):
    print(_client_creation_request.dict(by_alias=True))
    r = client.post("/", json=_client_creation_request.dict(by_alias=True), headers=auth(session_secret))

    assert r.status_code == status.HTTP_201_CREATED


def test_post_bad_request_multiple_sessions(session_secret: str):
    client.post("/", json=_client_creation_request.dict(by_alias=True), headers=auth(session_secret))
    r = client.post("/", json=_client_creation_request.dict(by_alias=True), headers=auth(session_secret))
    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_put_bad_request_on_unknown_pseudonym(session_secret: str, pseudonyms: List[str]):
    # create client secret
    client.post("/", json=_client_creation_request.dict(by_alias=True), headers=auth(session_secret))
    # create PUT request with nonsensical pseudonym
    r = client.put("/", json=pseudonyms + ["foobar"], headers=auth(session_secret))

    assert r.status_code == status.HTTP_502_BAD_GATEWAY


def test_put(session_secret: str, pseudonyms: List[str]):
    # create client secret
    client.post("/", json=_client_creation_request.dict(by_alias=True), headers=auth(session_secret))
    # create PUT request
    r = client.put("/", json=pseudonyms, headers=auth(session_secret))

    assert r.status_code == status.HTTP_202_ACCEPTED


def test_get_bad_request_not_registered():
    r = client.get("/", json=[], headers=auth("foobar"))
    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_get(session_secret: str, pseudonyms: List[str]):
    broker = BrokerClient(get_broker_base_url())

    # create client secret
    client.post("/", json=_client_creation_request.dict(by_alias=True), headers=auth(session_secret))
    # create PUT request
    client.put("/", json=pseudonyms, headers=auth(session_secret))

    # create a dummy client at the broker
    other_client_secret = broker.create_client(session_secret)
    # submit an example bit vector on behalf of that other client
    dt = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(days=1)

    broker.submit_bit_vectors(other_client_secret, [
        BitVector(
            id=secrets.token_urlsafe(32),
            value="Zm9vYmFy",
            metadata=[
                BitVectorMetadata(
                    name="createdAt",
                    value=dt.isoformat(timespec="seconds")  # this timestamp > epix creation timestamp
                )
            ]
        )
    ])

    end_time = time.time() + get_service_timeout_secs()

    while True:
        if broker.get_session_progress(session_secret) == 1:
            break

        if time.time() >= end_time:
            raise ValueError(f"Matching didn't finish after {get_service_timeout_secs()} seconds")

        time.sleep(1)

    # retrieve results from resolver
    r = client.get("/", headers=auth(session_secret))

    for m in PseudonymMatchResponseModel(**r.json()).match_list:
        v = m.vector

        assert v.id in pseudonyms
        assert v.metadata != m.reference_metadata
