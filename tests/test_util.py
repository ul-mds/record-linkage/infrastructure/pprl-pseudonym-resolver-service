from pprl_resolver.util import snake_to_camel, camel_to_snake


def test_snake_to_camel():
    assert snake_to_camel(["foobar", "foo_bar", "__foo_bar__"]) == ["foobar", "fooBar", "fooBar"]


def test_camel_to_snake():
    assert camel_to_snake(["foobar", "fooBar", "FooBar"]) == ["foobar", "foo_bar", "foo_bar"]
