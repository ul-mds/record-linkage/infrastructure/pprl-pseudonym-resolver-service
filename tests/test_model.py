from pprl import BitVector, BitVectorMetadata, BitVectorMetadataSpecification, BitVectorMatch, \
    BloomFilterConfiguration, AttributeSchema

from pprl_resolver.model import BitVectorMetadataModel, BitVectorModel, BitVectorMetadataSpecificationModel, \
    BitVectorMatchModel, BloomFilterConfigurationModel, AttributeSchemaModel, ClientCreationRequestModel


def _test_metadata_eq(orm: BitVectorMetadata, model: BitVectorMetadataModel):
    assert orm.name == model.name
    assert orm.value == model.value


def test_bit_vector_metadata_orm():
    meta_orm = BitVectorMetadata(name="foo", value="bar")
    meta = BitVectorMetadataModel.from_orm(meta_orm)

    _test_metadata_eq(meta_orm, meta)


def _test_vector_eq(orm: BitVector, model: BitVectorModel):
    assert orm.id == model.id
    assert orm.value == model.value
    assert len(orm.metadata) == len(model.metadata)

    for i in range(len(orm.metadata)):
        _test_metadata_eq(orm.metadata[i], model.metadata[i])


def test_bit_vector_orm():
    bv_orm = BitVector(
        id="1",
        value="foobar",
        metadata=[BitVectorMetadata(name="foo", value="bar")]
    )
    bv = BitVectorModel.from_orm(bv_orm)

    _test_vector_eq(bv_orm, bv)


def _test_spec_eq(orm: BitVectorMetadataSpecification, model: BitVectorMetadataSpecificationModel):
    assert orm.name == model.name
    assert orm.data_type == model.data_type
    assert orm.decision_rule == model.decision_rule


def test_spec_orm():
    meta_spec_orm = BitVectorMetadataSpecification(
        name="createdAt",
        data_type="datetime",
        decision_rule="keepLatest"
    )
    meta_spec = BitVectorMetadataSpecificationModel.from_orm(meta_spec_orm)

    _test_spec_eq(meta_spec_orm, meta_spec)


def _test_match_eq(orm: BitVectorMatch, model: BitVectorMatchModel):
    _test_vector_eq(orm.vector, model.vector)
    assert orm.similarity == model.similarity
    assert len(orm.reference_metadata) == len(model.reference_metadata)

    for i in range(len(orm.reference_metadata)):
        _test_metadata_eq(orm.reference_metadata[i], model.reference_metadata[i])


def test_vector_match_orm():
    match_orm = BitVectorMatch(
        vector=BitVector(id="foo", value="bar", metadata=[BitVectorMetadata(name="foo", value="bar")]),
        similarity=.5,
        reference_metadata=[BitVectorMetadata(name="foo", value="baz")]
    )
    match = BitVectorMatchModel.from_orm(match_orm)

    _test_match_eq(match_orm, match)


def _test_bloom_eq(orm: BloomFilterConfiguration, model: BloomFilterConfigurationModel):
    for a in ("charset", "filter_type", "hash_strategy", "hash_values", "token_size", "seed", "key", "salt"):
        assert getattr(orm, a) == getattr(model, a)


def test_bloom_orm():
    bf_orm = BloomFilterConfiguration(
        charset="UTF-8",
        filter_type="RBF",
        hash_strategy="RANDOM_SHA256",
        hash_values=5,
        token_size=2,
        seed=1337,
        key="s3cr3t",
        salt="s4lty"
    )
    bf = BloomFilterConfigurationModel.from_orm(bf_orm)

    _test_bloom_eq(bf_orm, bf)


def _test_schema_eq(orm: AttributeSchema, model: AttributeSchemaModel):
    for a in ("attribute_name", "data_type", "average_token_count", "weight"):
        assert getattr(orm, a) == getattr(model, a)


def test_schema_orm():
    schema_orm = AttributeSchema(
        attribute_name="foo",
        data_type="bar",
        average_token_count=2,
        weight=3
    )
    schema = AttributeSchemaModel.from_orm(schema_orm)

    _test_schema_eq(schema_orm, schema)


def test_client_creation_to_local_cache():
    # this is what an incoming request would look like (minus the many empty lists)
    req = ClientCreationRequestModel(**{
        "bloomFilter": {},
        "clientConfig": {
            "domain": "default",
            "attributes": ["foo"]
        },
        "schemas": [],
        "metadataSpecification": []
    })

    secret = "secret"
    cache = req.to_local_session_cache(secret)

    for a in ("bloom_filter", "client_config", "schema_list", "metadata_spec_list"):
        assert getattr(req, a) == getattr(cache, a)

    assert cache.client_secret == secret
