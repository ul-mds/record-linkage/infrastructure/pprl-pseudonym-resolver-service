import os
import time
from functools import lru_cache

import requests
from requests import RequestException

from pprl_resolver.config import Settings


@lru_cache
def get_service_timeout_secs() -> int:
    return int(os.getenv("PYTEST_SERVICE_TIMEOUT", 20))


def _wait_service_healthy(uri: str) -> str:
    timeout_secs = get_service_timeout_secs()
    end_time = time.time() + timeout_secs

    while True:
        try:
            requests.get(uri).raise_for_status()
            return uri
        except RequestException as e:
            if time.time() >= end_time:
                raise ValueError(f"failed to connect to {uri} after {timeout_secs} seconds: {e}")

            time.sleep(1)


def _get_env_or_raise(e: str) -> str:
    v = os.getenv(e)

    if v is None:
        raise ValueError(f"environment variable {e} is not set")

    return v


@lru_cache
def get_epix_wsdl_url() -> str:
    return _wait_service_healthy(_get_env_or_raise("PYTEST_EPIX_WSDL_URL"))


@lru_cache
def get_gpas_wsdl_url() -> str:
    return _wait_service_healthy(_get_env_or_raise("PYTEST_GPAS_WSDL_URL"))


@lru_cache
def get_broker_base_url() -> str:
    return _wait_service_healthy(_get_env_or_raise("PYTEST_BROKER_BASE_URL"))


@lru_cache
def get_encoder_base_url() -> str:
    return _wait_service_healthy(_get_env_or_raise("PYTEST_ENCODER_BASE_URL"))


@lru_cache
def get_domain_name() -> str:
    return os.getenv("PYTEST_DOMAIN_NAME", "default")


@lru_cache
def get_epix_source_name() -> str:
    return os.getenv("PYTEST_EPIX_SOURCE_NAME", "dummy_safe_source")


def get_settings_override():
    return Settings(
        broker_base_url=get_broker_base_url(),
        encoder_base_url=get_encoder_base_url(),
        epix_wsdl_url=get_epix_wsdl_url(),
        gpas_wsdl_url=get_gpas_wsdl_url()
    )
