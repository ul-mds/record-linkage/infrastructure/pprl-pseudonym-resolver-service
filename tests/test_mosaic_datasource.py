import pytest

from pprl_resolver.datasources import MosaicDataSource
from .test_config import get_settings_override


pytestmark = pytest.mark.integration


@pytest.fixture
def datasource() -> MosaicDataSource:
    return MosaicDataSource(get_settings_override())


def test_get_available_attributes(datasource: MosaicDataSource):
    assert len(datasource.get_available_attributes()) != 0


def test_get_available_metadata_sources(datasource: MosaicDataSource):
    assert len(datasource.get_available_metadata_sources()) != 0
